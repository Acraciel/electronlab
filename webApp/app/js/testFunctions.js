// Handler when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", function(){

  notifyMe();
});

/*Functions for notifications Chrome*/

function notifyMe() {
  //Se Verifica que el navegador soporte notificaciones
  if (!("Notification" in window)) {
    alert("El Navegador actual no soporta las notificaciones");
  }

  //Verificamos cuando se ha permitido envíar notificaciones
  else if (Notification.permission === "granted") {
    //Si todo esta OK, envíamos una notificación
    var notification = new Notification("Notificaciones Activadas!");
  }

  //De otra manera se necesita pedir permiso.
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // Si acepta, envíamos un mensaje.
      if (permission === "granted") {
        var notification = new Notification("Notificaciones Aceptadas!");
      }
    });
  }

//Por último, si niegan las notificaciones. No molestamos con el permiso.
};
if(Notification.permission === "denied")
{
  Notification.requestPermission().then(function(result) {
    console.log(result);
  });
}

//Si esta todo activado con este método envío notificaciones
function spawnNotification(theBody,theIcon,theTitle) {
  var options = {
      body: theBody,
      icon: theIcon
  }
  var n = new Notification(theTitle,options);
}

/* ./END Functions for notifications Chrome*/
