// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const path = require('path');
const remote = require('electron').remote;
const app = remote.app;

//Evita que puedan hacer zoom en la aplicación
var webFrame = require('electron').webFrame;
webFrame.setZoomLevelLimits(1, 1);

//Borrado de archivos (caché en este caso) usando NodeJs y Electron
function deleteCookies() {
    var chromeCacheDir = path.join(app.getPath('userData')); 
    if(fs.existsSync(chromeCacheDir)) {
        var files = fs.readdirSync(chromeCacheDir);
        for(var i=0; i<files.length; i++) {
        	if(files[i] == 'Cookies')
        	{
        		var filename = path.join(chromeCacheDir, files[i]);
	            if(fs.existsSync(filename)) {
	                try {                	
	                    fs.unlinkSync(filename);
	                    return 'ok';
	                }
	                catch(e) {
	                	return e;
	                }
	            }
        	}
            
        }
    }
};
