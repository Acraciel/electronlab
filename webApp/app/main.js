const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

//Module remote
const remote = electron.remote;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

app.commandLine.appendSwitch('--enable-viewport-meta', 'true');

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        frame: true,
        fullscreen: false,
        width: 900,
        height: 800,
        minHeight: 500,
        minWidth: 700,
        resizable: true,
        autoHideMenuBar: true,
        title: 'ElectronLab',
        icon: `${__dirname}/images/code.ico`
    });

    // and load the index.html of the app.
    //mainWindow.loadURL(`file://${__dirname}/index.html`); //Para usar local
    mainWindow.loadURL("https://web.whatsapp.com/");//Embeber una web externa

    // Open the DevTools.
    //mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    });


    mainWindow.webContents.on('crashed', function() {
        const options = {
            type: 'info',
            title: 'Renderer Process Crashed',
            message: 'This process has crashed.',
            buttons: ['Reload', 'Close']
        };
        dialog.showMessageBox(options, function(index) {
            if (index === 0) win.reload();
            else win.close();
        });
    });


};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    };
});

app.on('activate', function() {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    };
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
