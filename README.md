![](webApp/app/images/summer8bits.jpg)
# ElectronLab

Creada para experimentar con electron, la cual ya trae por defecto configuraciones básicas y un demo basado en Whatsapp, el cual podemos cambiar por un mini proyecto de pruebas locales.

**Para poder hacer andar el proyecto necesitamos tener instalado previamente [NodeJs](https://nodejs.org/es/), con el cual podremos utilizar los comandos para inicializar el proyecto con sus dependencias.**

## Contiene

- Web ("Whatsapp") envevida por defecto.
- Basic Web (HTML+JS+CSS) que incluye:
  - Prueba de notificaciones

**Para cambiar a la web local se debe modificar el archivo "main.js", que se encuentra dentro de [webApp/app]**
```js
/*[Ln:32 - 33] Comentamos web.whatsapp para dejar sin comentar la web local*/

mainWindow.loadURL(`file://${__dirname}/index.html`);
//mainWindow.loadURL("https://web.whatsapp.com/");   
```

## Inicializando el Proyecto

Comandos para instalar dependencias, ejecutar la aplicación o generar paquetes de distribución.

**"Se recomienda generar los build en cada S.O. respectivo, para evitar problemas"**

```bash
# Install dependencies
npm install
# Excecute app
npm start
# Dist Package
npm run buildWin    #Generate package of 32 and 64
npm run buildLinux  #Generate package of 64
```

## Dependencias Utilizadas

- [Electron](https://electron.atom.io/)
- [Electron-builder](https://github.com/electron-userland/electron-builder)
- [Electron-packager](https://github.com/electron-userland/electron-packager)

### License [MIT](LICENSE.md)
